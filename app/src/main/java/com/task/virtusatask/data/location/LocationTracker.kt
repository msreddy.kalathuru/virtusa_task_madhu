package com.task.virtusatask.data.location

import android.location.Location

fun interface LocationTracker {

    suspend fun getCurrentLocation(): Location?

}
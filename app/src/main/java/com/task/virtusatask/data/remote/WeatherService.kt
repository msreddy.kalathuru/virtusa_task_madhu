package com.task.virtusatask.data.remote

import com.task.virtusatask.data.model.CoordinatesResponse
import com.task.virtusatask.data.model.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Interface for Retrofit API Calls
 */
interface WeatherService {

    @GET("/data/2.5/weather")
    suspend fun getWeather(@QueryMap map: Map<String, String>): WeatherResponse

    @GET("/geo/1.0/direct")
    suspend fun getLatAndLong(@QueryMap map: Map<String, String>): List<CoordinatesResponse>

}
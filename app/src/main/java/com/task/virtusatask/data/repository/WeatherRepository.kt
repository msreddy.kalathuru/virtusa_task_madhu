package com.task.virtusatask.data.repository

import com.task.virtusatask.utils.Resource
import com.task.virtusatask.data.model.CoordinatesResponse
import com.task.virtusatask.data.model.WeatherResponse
import kotlinx.coroutines.flow.Flow

interface WeatherRepository {

    //ToGet Weather Information with map Lat, Lng
     fun getWeatherData(lat: String, long: String): Flow<Resource<WeatherResponse>>

    //ToGet Lat,Lng information with given city name
     fun getCoordinates(cityName: String): Flow<Resource<List<CoordinatesResponse>>>
}
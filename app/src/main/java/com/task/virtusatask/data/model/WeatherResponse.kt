package com.task.virtusatask.data.model

/**
 * Data class representing weather information.
 */
data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

/**
 * Data class representing main weather parameters.
 */
data class Main(
    val temp: Double,
    val feelsLike: Double,
    val tempMin: Double,
    val tempMax: Double,
    val pressure: Int,
    val humidity: Int
)

/**
 * Data class representing geographical coordinates.
 */
data class Coord(
    val lon: Double?,
    val lat: Double?
)

/**
 * Data class representing the last known location.
 */
data class LastLocation(
    val lon: String?,
    val lat: String?
)

/**
 * Data class representing wind information.
 */
data class Wind(
    val speed: Double,
    val deg: Int
)

/**
 * Data class representing cloudiness information.
 */
data class Clouds(
    val all: Int
)

/**
 * Data class representing system parameters.
 */
data class Sys(
    val type: Int,
    val id: Int,
    val country: String,
    val sunrise: Long,
    val sunset: Long
)

/**
 * Data class representing the weather response from the server.
 */
data class WeatherResponse(
    val coord: Coord,
    val weather: List<Weather>,
    val base: String,
    val main: Main,
    val visibility: Int,
    val wind: Wind,
    val clouds: Clouds,
    val dt: Long,
    val sys: Sys,
    val timezone: Int,
    val id: Int,
    val name: String,
    val cod: Int
)

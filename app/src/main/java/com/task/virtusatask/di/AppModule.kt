package com.task.virtusatask.di

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.task.virtusatask.data.location.LocationTracker
import com.task.virtusatask.data.location.LocationTrackerImpl
import com.task.virtusatask.data.remote.WeatherService
import com.task.virtusatask.data.repository.WeatherRepository
import com.task.virtusatask.data.repository.WeatherRepositoryImpl
import com.task.virtusatask.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

/**
 * Dependency Provider for Application Components
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    private const val LOCATION_FILE = "location.preferences_pb"

    /**
     * Provides Retrofit Instance
     */
    @Provides
    @Singleton
    fun provideWeatherServiceInterface(): WeatherService {
        return Retrofit.Builder().baseUrl(Constants.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create()).build()
            .create(WeatherService::class.java)
    }

    /**
     * Provides WeatherRepository Instance
     */

    @Provides
    @Singleton
    fun provideWeatherRepository(weatherRepositoryImpl: WeatherRepositoryImpl): WeatherRepository {
        return weatherRepositoryImpl
    }

    /**
     * Provides FusedLocationProviderClient Instance
     */

    @Provides
    @Singleton
    fun provideFusedLocationProviderClient(app: Application): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(app)
    }

    /**
     * Provides LocationTracker Instance
     */

    @Provides
    @Singleton
    fun provideLocationRepository(locationRepositoryImpl: LocationTrackerImpl): LocationTracker {
        return locationRepositoryImpl
    }

    /**
     * Provides DataStore Single Instance
     */
    @Singleton
    @Provides
    fun providePreferencesDataStore(@ApplicationContext appContext: Context): DataStore<Preferences> {
        return PreferenceDataStoreFactory.create(
            corruptionHandler = ReplaceFileCorruptionHandler(
                produceNewData = { emptyPreferences() }
            ),
            scope = CoroutineScope(Dispatchers.IO + SupervisorJob()),
            produceFile = { appContext.preferencesDataStoreFile(LOCATION_FILE) }
        )
    }

}
package com.task.virtusatask.viewmodel

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.task.virtusatask.data.location.LocationTracker
import com.task.virtusatask.data.repository.WeatherRepository
import com.task.virtusatask.utils.Constants.LAST_LAT
import com.task.virtusatask.utils.Constants.LAST_LONG
import com.task.virtusatask.utils.Resource
import com.task.virtusatask.view.components.WeatherHomeScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * WeatherViewModel is responsible for managing UI-related data for weather information.
 * Acts as a bridge between the UI (Jetpack Compose) and data sources (repositories and location tracker).
 * Uses ViewModel's lifecycle to handle data store and weather data fetching, ensuring data is retained during configuration changes.
 */
@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val locationTracker: LocationTracker,
    private val dataStore: DataStore<Preferences>
) : ViewModel() {
    // Holds the current state of the weather screen
    var weatherState by mutableStateOf(WeatherHomeScreenState())
    private var lat: String = ""
    private var long: String = ""
    private var searchJob: Job? = null

    /**
     * Retrieves saved latitude and longitude from DataStore and fetches weather data accordingly.
     * Invoked when the app loads to restore the last known location's weather data.
     */
    fun getLocationFromPreferences() {
        viewModelScope.launch {
            weatherState = weatherState.copy(isLoading = true)
            dataStore.data.collect { preferences ->
                preferences[LAST_LAT]?.let { lat ->
                    preferences[LAST_LONG]?.let { long ->
                        getWeatherData(lat, long)
                    }
                }
            }
        }
    }

    /**
     * Obtains the current device location if location permissions are granted.
     * Initiates weather data retrieval based on the current coordinates and stores them in DataStore.
     * Updates weather state to indicate loading status and handles cases when location is unavailable.
     */
    fun loadLocationFromCoordinates() {
        viewModelScope.launch {
            weatherState = weatherState.copy(isLoading = true)
            locationTracker.getCurrentLocation()?.let { location ->
                getWeatherData(location.latitude.toString(), location.longitude.toString())
                lat = location.latitude.toString()
                long = location.longitude.toString()
                storeCurrentLocation(lat, long)
            } ?: kotlin.run {
                weatherState = weatherState.copy(
                    isLoading = false,
                )
            }
        }

    }

    /**
     * Saves the current location (latitude and longitude) to DataStore for future retrieval.
     * Allows the app to persist the last searched or detected location for later use.
     *
     * @param lat Latitude to save in DataStore.
     * @param long Longitude to save in DataStore.
     */
    private fun storeCurrentLocation(lat: String, long: String) {
        viewModelScope.launch {
            dataStore.edit { data ->
                data[LAST_LAT] = lat
                data[LAST_LONG] = long
            }
        }
    }

    /**
     * Fetches weather data based on provided latitude and longitude coordinates.
     * Updates the weather state with the result of the weather API call.
     *
     * @param lat Latitude coordinate as a string.
     * @param long Longitude coordinate as a string.
     */
    fun getWeatherData(lat: String, long: String) {
        viewModelScope.launch {
            weatherRepository.getWeatherData(lat, long).collect { result ->
                when (result) {
                    is Resource.Error -> {
                        weatherState = weatherState.copy(
                            errorMessage = result.message
                        )
                    }

                    is Resource.Success -> {
                        result.data?.let { data ->
                            weatherState = weatherState.copy(
                                weatherResponse = data
                            )
                        }
                    }

                    is Resource.Loading -> {
                        weatherState = weatherState.copy(isLoading = result.isLoading)
                    }

                }
            }
        }

    }

    /**
     * Initiates a search for weather data based on a location query entered by the user.
     * Uses the query to retrieve coordinates and then fetches corresponding weather data.
     *
     * @param query The location query entered by the user, such as a city name.
     */
    fun makeWeatherAPICall(query: String) {
        weatherState = weatherState.copy(searchQuery = query)
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            delay(300)
            weatherRepository.getCoordinates(query).collect {
                when (it) {
                    is Resource.Error -> {
                        weatherState = weatherState.copy(
                            isLoading = false,
                            errorMessage = it.message
                        )
                    }

                    is Resource.Success -> {
                        it.data?.let { data ->
                            weatherState = weatherState.copy(
                                isLoading = false,
                                errorMessage = null,
                                coordinatesResponse = data
                            )
                            getWeatherData(data[0].lat.toString(), data[0].lon.toString())
                            storeCurrentLocation(data[0].lat.toString(), data[0].lon.toString())
                            lat = data[0].lat.toString()
                            long = data[0].lon.toString()
                        }
                    }

                    is Resource.Loading -> {
                        weatherState = weatherState.copy(isLoading = it.isLoading)
                    }
                }
            }
        }
    }
}
package com.task.virtusatask.view.components

import com.task.virtusatask.data.model.CoordinatesResponse
import com.task.virtusatask.data.model.WeatherResponse

/**
 * Data class for WeatherHomeScreenState
 */
data class WeatherHomeScreenState(
    val weatherResponse: WeatherResponse? = null,
    val coordinatesResponse: List<CoordinatesResponse>? = null,
    val isLoading: Boolean = false,
    val errorMessage: String? = "",
    val searchQuery: String = ""
)

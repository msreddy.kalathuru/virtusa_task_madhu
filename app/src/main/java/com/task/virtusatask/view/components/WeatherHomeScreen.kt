package com.task.virtusatask.view.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.task.virtusatask.utils.Constants.SEARCH_ALLOW
import com.task.virtusatask.utils.Constants.SEARCH_WAIT_TIME
import com.task.virtusatask.view.theme.DarkBlue
import com.task.virtusatask.viewmodel.WeatherViewModel
import com.task.virtusaweatherapp.R
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce

@Composable
fun WeatherHomeScreen(viewModel: WeatherViewModel = hiltViewModel()) {
    val searchText = remember { mutableStateOf("") }
    val focusManager = LocalFocusManager.current

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(DarkBlue),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            VistusaSearchBar(
                searchText = searchText,
                viewModel = viewModel,
                focusManager = focusManager
            )
            SearchStates(viewModel, searchText, focusManager)
            ResponseStates(viewModel)
        }
    }
}

@OptIn(FlowPreview::class)
@Composable
fun SearchStates(
    viewModel: WeatherViewModel,
    searchText: MutableState<String>,
    focusManager: FocusManager
) {
    LaunchedEffect(searchText) {
        snapshotFlow { searchText.value }
            .debounce(SEARCH_WAIT_TIME) // Adjust the debounce time as needed
            .collectLatest { query ->
                if (query.isNotEmpty()) {
                    viewModel.makeWeatherAPICall(query)
                    focusManager.clearFocus()
                }
            }
    }
}

@Composable
fun ResponseStates(
    viewModel: WeatherViewModel
) {
    when {
        viewModel.weatherState.isLoading -> {
            Box(
                modifier = Modifier
                    .fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator()
                Text("Loading... Please wait.", color = Color.White)
            }
        }

        viewModel.weatherState.weatherResponse != null -> {
            viewModel.weatherState.weatherResponse?.let { weatherResponse ->
                if (weatherResponse.sys.country == SEARCH_ALLOW) {
                    VirtusaDetailsCard(weatherResponse = weatherResponse)
                } else {

                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Image(
                                painter = painterResource(id = R.drawable.data_notfound),
                                contentDescription = "Sample Image",
                                modifier = Modifier.size(100.dp)
                            )
                            Text(
                                text = "Please search only states in USA",
                                textAlign = TextAlign.Center,
                                color = Color.Red
                            )

                        }
                    }
                }
            }
        }

        viewModel.weatherState.errorMessage?.isNotEmpty() == true -> {
            viewModel.weatherState.errorMessage?.let { error ->
                VirtusaErrorCard(
                    imagePainter = painterResource(id = R.drawable.data_notfound),
                    text = error
                )
            }
        }
    }
}




package com.task.virtusatask.view.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.capitalize
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.task.virtusatask.data.model.WeatherResponse
import com.task.virtusatask.utils.Utils
import com.task.virtusatask.view.theme.Black
import com.task.virtusatask.view.theme.DeepBlue
import com.task.virtusatask.view.theme.White
import com.task.virtusatask.viewmodel.WeatherViewModel
import com.task.virtusaweatherapp.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VistusaSearchBar(
    searchText: MutableState<String>,
    viewModel: WeatherViewModel,
    focusManager: FocusManager
) {
    Box(
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopCenter
    ) {
        OutlinedTextField(
            value = searchText.value,
            onValueChange = { searchText.value = it },
            modifier = Modifier
                .padding(horizontal = 16.dp, vertical = 8.dp)
                .fillMaxWidth()
                .background(Color.White, shape = RoundedCornerShape(8.dp)),
            placeholder = {
                Text(
                    text = "Search location here...",
                    color = Color.Gray,
                    style = MaterialTheme.typography.bodyMedium
                )
            },
            maxLines = 1,
            singleLine = true,
            keyboardActions = KeyboardActions(onSearch = {
                viewModel.makeWeatherAPICall(searchText.value)
                focusManager.clearFocus()
            }),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = MaterialTheme.colorScheme.primary,
                unfocusedBorderColor = Color.LightGray,
                cursorColor = MaterialTheme.colorScheme.primary
            ),
            shape = RoundedCornerShape(8.dp)
        )
    }
}

@Composable
fun VirtusaText(text: String, fontSize: TextUnit = 16.sp, color: Color, textAlignment: TextAlign) {
    Text(
        text = text,
        fontWeight = FontWeight.Bold,
        fontSize = fontSize,
        color = color,
        overflow = TextOverflow.Ellipsis,
        maxLines = 1,
        textAlign = textAlignment
    )
}

@Composable
fun VirtusaDetailRow(label: String, value: String? = null, color: Color) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp)
    ) {
        VirtusaText(label, color = color, textAlignment = TextAlign.Left)
        VirtusaText(
            value ?: "N/A",
            color = color,
            textAlignment = TextAlign.Right
        ) // Display "N/A" if the value is null or empty
    }
}

@Composable
fun VirtusaIconWithTempRow(iconId: String, modifier: Modifier) {
    Image(
        painter = rememberAsyncImagePainter(
            ImageRequest.Builder
                (LocalContext.current)
                .data(data = "https://openweathermap.org/img/wn/${iconId}@2x.png")
                .apply(block = fun ImageRequest.Builder.() {
                    placeholder(R.drawable.data_notfound)
                    error(R.drawable.data_notfound)
                }).build()
        ),
        contentDescription = "Weather Icon",
        modifier = modifier,
    )
}

@Composable
fun VirtusaDetailsCard(weatherResponse: WeatherResponse) {
    Card(
        shape = RoundedCornerShape(10.dp),
        colors = CardDefaults.cardColors(containerColor = White),
        modifier = Modifier
            .wrapContentSize()
            .padding(16.dp)
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            // Text content aligned to the top left
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 16.dp), // Add space between text and icon
                horizontalAlignment = Alignment.Start
            ) {
                VirtusaText(
                    "Today",
                    fontSize = 18.sp,
                    color = Black,
                    textAlignment = TextAlign.Left
                )
                VirtusaText(
                    "${weatherResponse.name}, ${weatherResponse.sys.country}",
                    fontSize = 28.sp,
                    color = DeepBlue,
                    textAlignment = TextAlign.Left
                )
                VirtusaDetailRow(
                    "Feels like",
                    "${Utils.convertTemperatureToCelsius(weatherResponse.main.feelsLike)}°C",
                    color = Black
                )
                VirtusaDetailRow(weatherResponse.weather[0].main, color = Black)
                VirtusaDetailRow(
                    weatherResponse.weather[0].description.capitalize(Locale.current), color = Black
                )
                VirtusaDetailRow(
                    "Temp max",
                    "${Utils.convertTemperatureToCelsius(weatherResponse.main.tempMax)}°C",
                    color = Black
                )
                VirtusaDetailRow(
                    "Temp min",
                    "${Utils.convertTemperatureToCelsius(weatherResponse.main.tempMin)}°C",
                    color = Black
                )
                VirtusaDetailRow(
                    "Humidity",
                    "${weatherResponse.main.humidity}%",
                    color = Black
                )
                VirtusaDetailRow(
                    "Visibility",
                    "${Utils.convertToDecimal(weatherResponse.visibility)} km",
                    color = Black
                )
            }

            // Aligning the Weather Icon to the bottom right
            VirtusaIconWithTempRow(
                iconId = weatherResponse.weather[0].icon,
                modifier = Modifier
                    .align(Alignment.TopEnd) // Align the icon to the bottom end
                    .size(60.dp) // Set the size as needed
            )
        }
    }
}

@Composable
fun VirtusaErrorCard(imagePainter: Painter, text: String) {
    Column (
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth() // Centers content horizontally within the Row
    ) {
        Image(
            painter = imagePainter,
            contentDescription = "Error Icon",
            modifier = Modifier
                .size(100.dp)
                .align(Alignment.CenterHorizontally)
        )
        Spacer(modifier = Modifier.width(8.dp)) // Changed to width for horizontal spacing
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = Color.Black,
            modifier = Modifier.align(Alignment.CenterHorizontally)
        )
    }
}

package com.task.virtusatask.utils

import androidx.datastore.preferences.core.stringPreferencesKey

object Constants {

    const val BASE_URL = "https://api.openweathermap.org"
    const val API_KEY = "" //Add your API key to use
    const val SEARCH_ALLOW = "US"
    val LAST_LAT = stringPreferencesKey("last_lat")
    val LAST_LONG = stringPreferencesKey("last_long")
    const val SEARCH_WAIT_TIME: Long = 1000
}
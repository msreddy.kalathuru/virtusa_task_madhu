package com.task.virtusatask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Application Class
 */
@HiltAndroidApp
class VirtusaWeatherApplication : Application() {
    override fun onCreate() {
        super.onCreate()

    }

}
package com.task.virtusatask.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.preferencesOf
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.task.virtusatask.data.location.LocationTracker
import com.task.virtusatask.data.model.CoordinatesResponse
import com.task.virtusatask.data.repository.WeatherRepository
import com.task.virtusatask.utils.Constants.LAST_LAT
import com.task.virtusatask.utils.Constants.LAST_LONG
import com.task.virtusatask.utils.Constants.SEARCH_ALLOW
import com.task.virtusatask.utils.Resource
import com.task.virtusatask.view.components.VirtusaDetailsCard
import com.task.virtusatask.view.components.VirtusaErrorCard
import com.task.virtusaweatherapp.R
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var viewModel: WeatherViewModel
    private val weatherRepository = mockk<WeatherRepository>(relaxed = true)
    private val locationTracker = mockk<LocationTracker>(relaxed = true)
    private val dataStore = mockk<DataStore<Preferences>>(relaxed = true)
    private val testDispatcher = StandardTestDispatcher()


    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        viewModel = WeatherViewModel(weatherRepository, locationTracker, dataStore)

    }


    @Test
    fun viewModelShouldNotBeNull() {
        assertNotNull(viewModel)
    }
    @Test
    fun repositoryShouldNotBeNull() {
        assertNotNull(weatherRepository)
    }
    @Test
    fun dataStoreShouldNotBeNull() {
        assertNotNull(dataStore)
    }

    @Test
    fun getWeatherState() {
    }

    @Test
    fun setWeatherState() {
    }

    @Test
    fun getLocationFromPreferences() {

    }

    @Test
    fun loadLocationFromCoordinates() {
    }

    @Test
    fun getWeatherData() {
    }

    @Test
    fun makeWeatherAPICall() {

    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
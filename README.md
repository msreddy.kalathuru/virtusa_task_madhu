# Virtusa Weather Application Task

This is Android based weather Application , we can able to see weather information by searching by city,state,country

## Table of Contents

   - Project Overview
   - Features
   - Getting Started
   - Prerequisites
   - Installation


## Project Overview

This is Android based weather Application , we can able to see weather information by searching by city,state,country

We are using https://api.openweathermap.org/data/2.5/weather?q={city name},{state code},{country code}&appid={API key}  to get proper weather information to display into android screen

**Note** : Add your own API key to use app

## Features

List the key features of your project:

- Get Coordinates with Cityname,State,country
- Get weather information with Coordinates

## Getting Started

Provide instructions on how to get your project up and running on a local machine.

### Prerequisites

List any software, libraries, or dependencies that need to be installed before running your project.
  
-Kotlin
-MVVM
-Retrofit
-Jetpack Compose
-Datastore
-Kotlin Coroutines
-Coil
-Maps

### Installation

Step-by-step guide on how to install your project:



Note : After Word Type 10m seconds after api will auomatically trigger

1. Clone this repository: `git clone https://gitlab.com/msreddy.kalathuru/jpmc_task_madhu.git`
2. Change to the project directory: `cd directoryName`
3. Install dependencies: `npm install` or `pip install -r requirements.txt`
4. ...

APK Installation steps
 -.apk file location JPMCWeatherApplication\apk...
 -Download and install in you android mobile
